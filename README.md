# PL-LoggerFramework

A framework used to log all actions occur in your application.
Based on : SwiftyBeaver

Best Mobile App Framework for logging
https://github.com/SwiftyBeaver/SwiftyBeaver

It have desktop version for tracking. check this website carefully.
https://swiftybeaver.com/ 

# Usage:
Show Sample Project


        // create logger object and add its logging ways
        // should be declared as shared instance in appdelegate
        let logger = PL_Logger()
            // display verbose messages in xcode console
            .activateConsole(minLevel: .verbose, format: .Message)
            // save info messages in file saved in "/Library/Caches/\(appName)"
            .activateFile(minLevel: .info, format: .Message)
            // send error messages to cloud server with these credentials
            .activateCloud(minLevel: .error, format: .Message, appID: "******", appSecret: "********************************", encryptionKey: "********************************")
        
        // display logging samples
        logger.build().verbose("verbose: ")
        logger.build().debug("debug: ")
        logger.build().info("info: ")
        logger.build().warning("warning: ")
        logger.build().error("error: ")

