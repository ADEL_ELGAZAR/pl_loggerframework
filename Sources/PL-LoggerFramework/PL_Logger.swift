//
//  PL_Logger.swift
//  PL_LoggerFramework
//
//  Created by FAB LAB on 10/21/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import Foundation

/// Builder Design Pattern
public final class PL_Logger {
    
    private var console:BaseDestination?
    private var file:BaseDestination?
    private var cloud:BaseDestination?
    private var google:BaseDestination?

    public init() { }
    
    // add console destination to logger container
    public func activateConsole(minLevel: SwiftyBeaver.Level, format: MsgFormat) -> PL_Logger{
        console = ConsoleDestination()
        
        // use custom format and set console output to short time, log level & message
        console!.format = format.rawValue
        console!.minLevel = minLevel

        return self
    }
    
    // add file destination to logger container
    public func activateFile(minLevel: SwiftyBeaver.Level, format: MsgFormat) -> PL_Logger{
        file = FileDestination()
        
        file!.format = format.rawValue
        file!.minLevel = minLevel
        
        return self
    }
    
    // add App cloud destination to logger container
    public func activateCloud(minLevel: SwiftyBeaver.Level, format: MsgFormat, appID: String, appSecret: String, encryptionKey: String) -> PL_Logger{
        cloud = SBPlatformDestination(appID: appID, appSecret: appSecret, encryptionKey: encryptionKey)
        
        cloud!.format = format.rawValue
        cloud!.minLevel = minLevel
        
        return self
    }
    
    // add google cloud destination to logger container
    public func activateGoogleCloud(minLevel: SwiftyBeaver.Level, format: MsgFormat, serviceName: String) -> PL_Logger{
        google = GoogleCloudDestination(serviceName: serviceName)
        
        google!.format = format.rawValue
        google!.minLevel = minLevel
        
        return self
    }
    
    // create new instance of logger container containing all destinations and return it
    public func build() -> SwiftyBeaver.Type {
        let logger = SwiftyBeaver.self
        
        if let console = console {
            logger.addDestination(console)
        }
        if let file = file {
            logger.addDestination(file)
        }
        if let cloud = cloud {
            logger.addDestination(cloud)
        }
        if let google = google {
            logger.addDestination(google)
        }
        
        return logger
    }
    
}

// the formet of displayed messages
public enum MsgFormat: String {
    case Message = "$DHH:mm:ss$d $L $M"
    case Json = "$J"
    case Context = "$L: $M $X"
}
