//
//  ViewController.swift
//  Sample
//
//  Created by FAB LAB on 11/6/19.
//  Copyright © 2019 FAB LAB. All rights reserved.
//

import UIKit
import PL_LoggerFramework

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create logger object and add its logging ways
        // should be declared as shared instance in appdelegate
        let logger = PL_Logger()
            // display verbose messages in xcode console
            .activateConsole(minLevel: .verbose, format: .Message)
            // save info messages in file saved in "/Library/Caches/\(appName)"
            .activateFile(minLevel: .info, format: .Message)
            // send error messages to cloud server with these credentials
            .activateCloud(minLevel: .error, format: .Message, appID: "Ybn1xP", appSecret: "xdzpffrHjR8jsldy86dAe6sk6snpenbc", encryptionKey: "jTkZg6sRwxpkM495ughn2msvYmgOj769")
        
        // display logging samples
        logger.build().verbose("verbose: ")
        logger.build().debug("debug: ")
        logger.build().info("info: ")
        logger.build().warning("warning: ")
        logger.build().error("error: ")
    }


}

