import XCTest

import PL_LoggerFrameworkTests

var tests = [XCTestCaseEntry]()
tests += PL_LoggerFrameworkTests.allTests()
XCTMain(tests)
